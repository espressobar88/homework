"use strict";
function createNewUser() {
  let newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your Surname"),
    birthday: prompt("Enter your Date", ["dd. mm. yyyy"]),
    passwordUser: prompt("Enter your Password"),
    getLogin: function () {
      return `${this.firstName[0]}${this.lastName.toLowerCase()}`;
    },
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
    getAge: function () {
      let userYear = new Date().getFullYear();
      let yyyy = newUser.birthday.slice(6, 10);
      let userBirthD = userYear - yyyy;
      if (userBirthD <= 18) {
        return `Ваш возвраст ${userBirthD} лет, вы еще очень молоды`; // если это лишнее я исправлю )
      } else
        return `Ваш возвраст ${userBirthD} лет, пора становиться IT-Junior`; // если это лишнее я исправлю)
    },
  };
  if (
    newUser.firstName === null ||
    newUser.lastName === null ||
    newUser.birthday === null ||
    newUser.passwordUser === null ||
    newUser.firstName.trim() === "" ||
    newUser.lastName.trim() === "" ||
    newUser.passwordUser.trim === ""
  ) {
  } else return newUser;
}
let result = createNewUser();
console.log(result.getPassword());
console.log(result.getAge());
