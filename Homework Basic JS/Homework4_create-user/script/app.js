"use strict";
function createNewUser() {
  let newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your Surname"),
    getLogin: function () {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
  };
  if (
    newUser.firstName === null ||
    newUser.lastName === null ||
    newUser.firstName.trim() === "" ||
    newUser.lastName.trim() === ""
  ) {
    return createNewUser();
  } else return newUser;
}
console.log(createNewUser().getLogin());
