"use strict";
let wrapper = document.querySelector(".btn-wrapper");
function changeColor(e) {
  for (let btn of wrapper.children) {
    btn.style.backgroundColor = "black";
  }
  let targetKey = e.key.toUpperCase();
  for (let key of wrapper.children) {
    let currentKey = key.innerHTML.toUpperCase();
    if (targetKey == currentKey) {
      key.style.backgroundColor = "blue";
    }
  }
}
document.addEventListener("keydown", changeColor);
