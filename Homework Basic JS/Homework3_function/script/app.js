"use strict";

function culcSum(a, y, choice) {
  do {
    a = prompt("Enter first number");
  } while (a === null || isNaN(+a) || a.trim() === "" || typeof a === String);
  do {
    y = prompt("Enter second number");
  } while (y === null || isNaN(+y) || y.trim() === "" || typeof y === String);
  do {
    choice = prompt("Enter your sing");
  } while (choice.trim() === "" || choice === null || choice === Number);
  switch (choice) {
    case "/":
      console.log(a / y);
      break;
    case "*":
      console.log(a * y);
      break;
    case "-":
      console.log(a - y);
      break;
    case "+":
      console.log(+a + +y);
      break;
    default:
      return culcSum();
  }
}
culcSum();
