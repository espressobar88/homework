"use strict";

let userNumber,
  x = 0;
do {
  userNumber = prompt("Введи целое число");
} while (
  userNumber === null ||
  userNumber.trim() === "" ||
  isNaN(+userNumber) ||
  Number.isInteger(+userNumber) == false
);
for (x; x <= userNumber; x++) {
  if (x % 5 == 0 && userNumber >= 5) {
    console.log(x);
  }
}
if (userNumber < 5) {
  console.log("Sorry no numbers");
}
