"use strict";
function filterBy(array = [], typeUser) {
  return array.filter((item) => typeof item != typeUser);
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));
