"use strict";

const changeColor = document.querySelector(".change-color");
let color = localStorage.getItem("bgCol");

window.onload = function () {
  if (localStorage.getItem("bgCol") !== null) {
    color = localStorage.getItem("bgCol");
    document.getElementsByTagName("body")[0].style.backgroundColor = color;
  }

  changeColor.addEventListener("click", function () {
    document.getElementsByTagName("body")[0].style.backgroundColor = "green";
    localStorage.setItem("bgCol", "green");
    if (localStorage.getItem("bgCol") == color) {
      document.getElementsByTagName("body")[0].style.backgroundColor = color;
    }
    if (color == "white") {
      localStorage.setItem("bgCol", "green");
      color = "green";
    } else {
      localStorage.setItem("bgCol", "white");
      color = "white";
      document.getElementsByTagName("body")[0].style.backgroundColor = color;
    }
    console.log(color);
  });
};
