"use strict";

let inputFirst = document.getElementById("input-first");
let showIcon = document.getElementById("icon-first");
let showSlash = document.getElementById("icon-slash");
let inputSecond = document.getElementById("input-second");
let showIcon2 = document.getElementById("icon-second");
let showSlash2 = document.getElementById("icon-slash2");

function showHide() {
  if (inputFirst.type == "password") {
    inputFirst.type = "text";
    showSlash.style.display = "block";
  } else {
    showIcon.style.display = "block";
    inputFirst.type = "password";
    showSlash.style.display = "none";
  }
  return false;
}
function showHide2() {
  if (inputSecond.type == "password") {
    inputSecond.type = "text";
    showSlash2.style.display = "block";
  } else {
    showIcon2.style.display = "block";
    inputSecond.type = "password";
    showSlash2.style.display = "none";
  }
  return false;
}
const btn = document.getElementById("btn");
btn.addEventListener("click", function () {
  if (inputFirst.value === inputSecond.value) {
    alert("Добро пожаловать");
  } else {
    const span = document.querySelector(".error");
    span.style.display = "block";
    span.textContent;
    inputFirst.value = "";
    inputSecond.value = "";
  }
});
