"use strict";
const input = document.getElementById("input");
const inputErrorText = document.querySelector(".error-text");

input.addEventListener("blur", function () {
  if (input.value < 0 || input.value === "") {
    document.querySelector("#info").style.display = "none";
    input.style.borderColor = "red";
    inputErrorText.innerHTML = "Please enter correct price";
    input.style.backgroundColor = "";
  } else {
    input.style.borderColor = "";
    inputErrorText.innerHTML = "";
    document.querySelector("#info").style.display = "block";
    document.querySelector(
      ".infoText"
    ).textContent = `Текущая цена ${input.value}`;
    input.style.backgroundColor = "green";
  }
});
document.querySelector(".clearData").addEventListener("click", function () {
  document.querySelector(".infoText").textContent = "";
  document.querySelector("#info").style.display = "none";
  input.value = "";
  input.style.backgroundColor = "white";
});
