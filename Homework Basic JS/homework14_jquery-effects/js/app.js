"use strict";

$(document).ready(function () {
  $(".btn-slide").click(function () {
    $(".popular__posts").slideToggle("slow");
    $(this).toggleClass("active");
  });
});
function slowScroll(id) {
  let offset = 0;
  $("html, body").animate(
    {
      scrollTop: $(id).offset().top - offset,
    },
    1000
  );
  return false;
}
$(".btn-top").click(function () {
  if ($(document).scrollTop() > 0) {
    $("html, body").animate({ scrollTop: 0 }, 1000);
  } else {
    $("html, body").animate({ scrollTop });
  }
});
$(".btn-top").css("display", "none");
$(document).scroll(function () {
  if ($(document).scrollTop() > 800) {
    $(".btn-top").css("display", "block");
  } else {
    $(".btn-top").css("display", "none");
  }
});
