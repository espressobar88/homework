"use strict";

// let name;
// let age;
// let w;
// do {
//   name = prompt("Введите Имя");
// } while (name === null || name.trim() === "");

// do {
//   age = prompt("Сколько вам лет?");
// } while (age === null || age.trim() === "" || isNaN(+age));

// if (age < 18) {
//   alert("You are not allowed to visit this website.");
// } else if (age > 22) {
//   alert("Welcome" + " " + name);
// } else if (age >= 18 && age <= 22) {
//   w = confirm("Are you sure you want to continue?");
//   if (w == true) {
//     alert("Welcome" + " " + name);
//   } else {
//     w == false;
//     alert("You are not allowed to visit this website.");
//   }
// }
// console.log("request data...");

// const p = new Promise(function (resolve, reject) {
//   setTimeout(() => {
//     console.log("Preparing data...");
//     const backendData = {
//       server: "aws",
//       port: 2000,
//       status: "working",
//     };
//     resolve(backendData);
//   }, 2000);
// });

// p.then((data) => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       data.modified = true;
//       resolve(data);
//     }, 3000);
//   });
// })

//   .then((clientData) => {
//     clientData.fromPromise = true;
//     return clientData;
//   })
//   .then((data) => {
//     console.log("Modified", data);
//   })
//   .catch((err) => console.error("Error", err))
//   .finally(() => console.log("Finaly"));

// const sleep = (ms) => {
//   return new Promise((resolve) => {
//     setTimeout(() => resolve(), ms);
//   });
// };

// sleep(2000).then(() => console.log("after 2 sec"));

// sleep(3000).then(() => console.log("after 3 sec"));

// Promise.all([sleep(2000), sleep(5000)]).then(() => {
//   console.log("All promises");
// });

// Promise.race([sleep(2000), sleep(5000)]).then(() => {
//   console.log("Race promises");
// });

// let a = 10;

// console.log(a);

// let b = new Promise(function (resolve, reject) {
//   setTimeout(() => {
//     resolve((a = 2));
//   }, 2000);
// });
// b.then(function(){
//   console.log(a);
// })
// const promise = new Promise((resolve, reject) => {
//   throw new Error("some error...");
//   setTimeout(() => {
//     if (true) {
//       resolve("promise completed!");
//     } else {
//       reject();
//     }
//   }, 3000);
// });

// promise.then((data) => console.log(data)).catch(() => console.log(error));
const WRING_OUT_TIME = 500;
const SQUATTUNG_TIME = 200;
function wringOut(count) {
  return new Promise((resolve, reject) => {
    if (count > 100) {
      reject(new Error("Слишком много отжиманий, я устал"));
    }
    setTimeout(() => {
      resolve();
    }, count * WRING_OUT_TIME);
  });
}
function squatting(count) {
  return new Promise((resolve, reject) => {
    if (count > 120) {
      reject(new Error("У вас отвалилась нога"));
    }
    setTimeout(() => {
      resolve();
    }, count * SQUATTUNG_TIME);
  });
}
console.log("Начать тренировку");

wringOut(10)
  .then(() => {
    console.log("Отжался 10 раз");
    return squatting(20);
  })
  .then(() => {
    console.log("Присел 20 раз");
  })
  .then(() => {
    setTimeout(() => {
      console.log("Тренировка окончена");
    }, 2000);
  });
