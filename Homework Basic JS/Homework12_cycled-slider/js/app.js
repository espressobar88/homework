"use strict";

const btn_pause = document.querySelector("#gallery .buttons .pause");
const btn_play = document.querySelector("#gallery .buttons .play");
let images = document.querySelectorAll("#gallery .photos img");
let i = 0;

let timerSlade = setInterval("slide()", 3000);
function slide() {
  images[i].style.display = "none";
  i++;
  if (i >= images.length) {
    i = 0;
  }
  images[i].style.display = "block";
}

btn_play.addEventListener("click", function () {
  timerSlade = setInterval("slide()", 3000);
  btn_play.style.display = "none";
});
btn_pause.addEventListener("click", function () {
  clearInterval(timerSlade);
  btn_play.style.display = "block";
});
