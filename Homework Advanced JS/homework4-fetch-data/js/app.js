"use strict";
let content;
let key;
let list = document.querySelector(".list");
function getFilms() {
  //Фукция верни нам ответ на GET запрос
  return fetch("https://swapi.dev/api/films/")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      //Давай присвоим результат данных которые мы обработали в переменную
      content = data.results;
      //Давай найдем все ключи в нашей переменной и вставим и выведем их
      for (key in content) {
        list.innerHTML += `<li>
                <h2> Эпиод № ${content[key].episode_id}</h2>
                <h3>
                ${content[key].title}
                </h3>
                <p class="cha-${key}"></p>
                <p>${content[key].opening_crawl}</p>
                </li>`;
      }
    });
}
//Функия может работать параллельно с функцией getFilms()
async function getActor() {
  //Подожем пезультат работы функции getFilms
  await getFilms();

  //Давай разделим массив на обьекты
  content.forEach((films, i) => {
    //Теперь будем вытаскивать актеров из каждого обьекта
    films.characters.forEach((elem) => {
      //Пришли ссылки =>  отправляй на обработку
      fetch(elem)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          //Получил имена , здорово , добавляй
          document.getElementsByClassName(`cha-${i}`)[0].append(data.name);
        });
    });
  });
}
//Вызывай одну функцию , она запустит вторую сама)
getActor();
