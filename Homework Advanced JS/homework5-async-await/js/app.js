"use strict";
let userIP;
let userCards;
let list = document.querySelector(".list");
const btnIp = document
  .querySelector(".button-ip")
  .addEventListener("click", getIP);

async function getIP() {
  await fetch("https://api.ipify.org/?format=json")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      userIP = data.ip;
    });
  getUserContact();
}

async function getUserContact() {
  await getIP;
  return fetch(`http://ip-api.com/json/${userIP}`, {
    mode: "cors",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      list.innerHTML = `<li><strong>Взломщик найден</strong></li><li>${data.timezone}</li><li>${data.country}</li><li>${data.region}</li><li>${data.city}</li><li>${data.regionName}</li>`;
    });
}
