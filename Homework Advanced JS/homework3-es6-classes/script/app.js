"use strict";

class Employee {
  constructor({ name = "Alex", age = 25, salary = 1000 }) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get userName() {
    return `${this.name}`;
  }
  get userAge() {
    return `${this.age}`;
  }
  get userSalary() {
    return `${this.salary}`;
  }
  set userName(newName) {
    this.name = newName;
  }
  set userAge(newAge) {
    this.age = newAge;
  }
  set userSalary(newSalary) {
    this.salary = newSalary;
  }
}

const user = new Employee({});

class Programmer extends Employee {
  constructor({
    lang = {
      ru: "Russian language",
      en: "English language",
      ua: "Ukrainian language",
    },
  }) {
    super(user);
    this.lang = lang;
  }

  get userSalary() {
    return this.salary * 3;
  }
}

const userHacker = new Programmer({});
class Admin extends Programmer {
  constructor({ age = 43, root = "admin", password = "0000" }) {
    super(userHacker);
    this.root = root;
    this.password = password;
    this.age = age;
  }
  get userName() {
    return `${(this.name = "Adminchik")}`;
  }
}
const userAdmin = new Admin({});
console.log(user);
console.log(userHacker);
console.log(userAdmin);
